# Hooq Email Service
A service that accepts input parameters for sending email and sends the email through automatic failover or chosen email provider.

### Description
- Do one thing well, send email.
- Provide abstractions for email service providers.
- Doesn't handle queue because it is receiving request from queueing service like NSQ or RabbitMQ or sending through service providers that is already handling queue
- Use standard URL e.g. http://localhost:8000/api/mail
- Uses JWT for API access to restrict public access, for some route we'll need to pass token
- Use proper RESTful HTTP (GET, POST, PUT and DELETE)
- Accept and Return JSON data
- Log all request to console

### Notes about this task
- tasks are below in TODO
- receiver email is fixed to hooq@protonmail.com because i'm using  sandbox/free account. :)
```js
url: protonmail.com
username: hooq
pass: hooq265
```
- for authentication, please use
```js
email: zufrizalyordan@gmail.com
pass: tralala
```
- For the backend i use NodeJS, my experience is not too advanced, only 1 year of usage. but in general, i have worked with Javascript more than 3 years.
- I choose ExpressJS because i've used it before, the code outside of ExpressJS and other packages/library is written by me.
- The database is using mongodb and acessing free mongo instance on mlab.com

##### Scalability concerns:
- Using 3rd party for db access make this service more scalable, since we're offloading the server resource, and also we can update/upgrade the db server without causing/less an issue with this mail service.
- We can use 3rd party or microservices for monitoring and logging to remove the server from doing extra work e.g. https://trace.risingstack.com

### API Endpoints
```js
POST /api/authenticate
params:
- email :String :required
- password :String :required
response:
- message
- token

POST /api/mail
params:
- subject :String :required
- receiver_name :String :required
- receiver_email :String :required
- status :String
- body :String :required
- token :String
```

### Installation
Use npm or yarn to install the packages
```js
yarn
```
or

```js
npm install
```

### Starting the server
```js
yarn start or npm start
```

#### Email provider examples:
- Mailgun
- ElasticEmail
- Mandrill -> Mailchimp Mail
- Amazon SES

#### Tech
1. ExpressJS for the app framework
2. Mocha & Chai for testing

#### TODO
- [x] Add test cases
- [x] Create mail service
- [x] Create & Use Mailgun mail service
- [x] Create & Use mail service failover
- [x] Create & Use ElasticEmail mail service
- [x] use JWT
- [x] DB - user - setup/seeding value
- [x] JWT user authentication
- [x] Use Token for API using JWT for security purposes
- [x] Save sent message to Mongo
- [x] Set up Front-end with React
- [x] Front-end -> Use material design component
- [x] Front-end -> List all messages
- [ ] Front-end -> Create message
- [ ] Front-end -> View message detail
- [ ] Front-end -> Delete message


#### Feature enhancements / additions
- [ ] Validate email address on mail service
- [ ] Create & Use Mailchimp mail service
- [ ] Create & Use Amazon SES mail service