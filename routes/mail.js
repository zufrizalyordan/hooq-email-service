// Author: zufrizalyordan@gmail.com
const mongoose = require('mongoose');
const MessageModel = require('../models/message');
const config = require('config');

let MailService = require('../services/mail');

// Use native promise
 mongoose.Promise = global.Promise;

/*
 * GET /api/mail to retrieve all the messages
 */
function getMessages(req, res) {
    const query = MessageModel.find({});
    query.exec((err, message) => {
        if(err) res.send(err);
        // If no errors, send result to client
        res.json(message);
    });
}

/*
 * GET /api/mail/:id to retrieve a message given its id.
 */
function getMessage(req, res) {
  MessageModel.findById(req.params.id, (err, message) => {
    if(err) res.send(err);
    res.json(message);
  });
}

function saveMessageToDb(data) {
  const newMessage = new MessageModel(data);
  newMessage.save()
    .catch(function(err) {
      if(config.util.getEnv('NODE_ENV') !== 'test') {
        console.log("Message not saved");
        console.log(err);
      }
    })
    .then(function (mssg) {
      if(config.util.getEnv('NODE_ENV') !== 'test') {
        console.log("Message saved");
      }
    });
}

/*
 * POST /api/mail to save a new message
 */
function postMessage(req, res) {
  MailService.send(req.query, res)
    .then(function(mssg) {
      if (mssg.message !== "") {
        const objs = Object.assign({}, req.query, {status: 'sent'});
        saveMessageToDb(objs);
      }

      return res.json(mssg);
    }).catch(function(err) {
      const objs = Object.assign({}, req.query, {status: 'not sent'});
      saveMessageToDb(objs);
      return res.json({ errors: err, message: "error" });
    });
}

module.exports = { getMessages, getMessage, postMessage };