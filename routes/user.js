// Author: zufrizalyordan@gmail.com
const mongoose = require('mongoose');
const User = require('../models/user');
const config = require('config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Use native promise
 mongoose.Promise = global.Promise;

/*
 * POST /api/authenticate to validate use login
 */
function authenticate(req, res) {
  let userEmail = req.query.email;
  let userPassword = req.query.password;

  if (req.body.hasOwnProperty('email')) {
    userEmail = req.body.email;
    userPassword = req.body.password;
  }

  User.findOne({
    email: userEmail
  }, (err, user) => {
    if (err) throw err;
    if (!user) {
      res.json({
        message: 'Authentication failed.',
        errors: {
            user: 'not_found'
          },
      });
    } else if (user) {
      var hash = user.password;
      bcrypt
        .compare(userPassword, hash, function(err, doesMatch){
          if (doesMatch) {
            var token = jwt.sign(user, config.JWT_SECRET, {
              expiresIn : 60*60*24
            });

            res.json({
              success: true,
              message: 'Token created',
              token: token
            });
          } else {
            res.json({
              message: 'Authentication failed.',
              errors: {
                password: 'wrong'
              },
            });
          }
        });
    }
  });
}

function seedAdmin() {
  return new global.Promise((resolve, reject) => {
    bcrypt.hash('tralala', 10, (err, hash) => {
      return new User({
        email: 'zufrizalyordan@gmail.com',
        name: 'mimin',
        password: hash,
        status: 'active',
        admin: true
      })
      .save((err) => {
        resolve({ message: 'admin saved' });
      })
      .catch((err) => {
        reject({ message: 'admin not saved' });
      });
    });
  });
}

function setup(req, res) {
  User
    .find({}, (err, user) => {
      if (user.length === 0) {
        seedAdmin()
          .then((resp) => {
            res.json(resp);
          })
          .catch((resp) => {
            res.json(resp);
          });
      } else {
        res.json({ message: 'admin already created'});
      }
    });
}

module.exports = { setup, authenticate };