const config = require('config');
const jwt = require('jsonwebtoken');

function tokenCheck(req, res, next) {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, config.JWT_SECRET, function(err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: 'Failed to authenticate token.'
        });
      } else {
        // save token request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).json({
        success: false,
        message: 'No token provided.'
    });
  }
}

module.exports = { tokenCheck };