import React from 'react';
import {render} from 'react-dom';
import AppShell from './app-shell';
import { MuiThemeProvider } from 'material-ui/styles';

const App = () => {
  return (
    <MuiThemeProvider>
    <AppShell />
    </MuiThemeProvider>
  )
}

render(<App/>, document.getElementById('app'));
