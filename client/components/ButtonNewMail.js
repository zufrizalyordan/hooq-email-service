// Author: zufrizalyordan@gmail.com
import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';

import Button from 'material-ui/Button';
import IconMail from 'material-ui-icons/Mail';

import { withStyles, createStyleSheet } from 'material-ui/styles';

const styleSheet = createStyleSheet('ButtonNewMail', {
  root: {
    position: 'absolute',
    bottom: '20px',
    right: '20px'
  },
});

const ButtonNewMail = (props) => {
  const classes = props.classes;
  const fabButton = (props.location.pathname !== '/new') ?
    <Button fab
      color="primary"
      className={classes.root}
      onClick={() => { props.history.push('/new') }}
    >
      <IconMail />
    </Button>
    : null;

  return fabButton;
}

ButtonNewMail.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styleSheet)(ButtonNewMail));