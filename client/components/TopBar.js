// Author: zufrizalyordan@gmail.com
// @flow
import React, {Component} from 'react';
import { Router, Route, Switch } from 'react-router';
import PropTypes from 'prop-types';
import createBrowserHistory from 'history/createBrowserHistory';

import { withStyles, createStyleSheet } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

const styleSheet = createStyleSheet('TopBar', {
  root: {
    flex: 1,
  },
});

const TopBar = (props) => {
  const classes = props.classes;

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography
          type="title"
          color="inherit"
          className={classes.root}>
          Hooq Mail Service
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

TopBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(TopBar);