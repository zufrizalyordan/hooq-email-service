// Author: zufrizalyordan@gmail.com
// @flow
import React, {Component} from 'react';
import { Router, Route, Switch } from 'react-router';
import PropTypes from 'prop-types';
import createBrowserHistory from 'history/createBrowserHistory';

import { withStyles, createStyleSheet } from 'material-ui/styles';

import TopBar from './components/Topbar';
import ButtonNewMail from './components/ButtonNewMail';

import Home from './screens/Home';
import MailForm from './screens/MailForm';
import NotFound from './screens/NotFound';

const styleSheet = createStyleSheet('AppShell', {
  root: {
    margin: '0 auto',
    maxWidth: 500,
    position: 'relative',
    width: '100%',
  }
});

const history = createBrowserHistory();

class AppShell extends Component {
  constructor() {
    super();

    this.state = {
      token: '',
      messages: []
    };

    this.authenticate();
  }

  loadMessages() {
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': this.state.token
      }
    }

    fetch('/api/mail', options)
      .then(response => response.json())
      .then(json => {
        const mssgs = json.map(function(val, idx) {
          return {
            subject: val.subject,
            name: val.receiver_name,
            email: val.receiver_email,
            id: val._id,
          }
        });

        this.setState({messages: mssgs});
      })
      .catch((err) => {
        console.log(err);
      })
  }

  authenticate() {
    const body = {
      email: "zufrizalyordan@gmail.com",
      password: "tralala"
    };

    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }

    fetch('/api/authenticate', options)
      .then(response => response.json())
      .then(json => {
        this.setState({token: json.token});
      })
      .then(() => {
        this.loadMessages();
      });
  }

  render() {
    const classes = this.props.classes;

    const HomeWrapper = () => {
      return <Home {...this.state} />;
    }

    return (
      <div className={classes.root}>
        <TopBar />
        <Router history={history}>
          <div>
            <Switch>
              <Route exact path="/"
                component={HomeWrapper}
              />
              <Route path="/new" component={MailForm} />
              <Route path="*" component={NotFound} />
            </Switch>
            <ButtonNewMail />
          </div>
        </Router>
      </div>
    );
  }
}

AppShell.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(AppShell);