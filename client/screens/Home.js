import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles, createStyleSheet } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

import Grid from 'material-ui/Grid';
import List, {
  ListItem,
  ListItemText,
} from 'material-ui/List';

const styleSheet = createStyleSheet('InteractiveList', theme => ({
  list: {
    background: theme.palette.background.paper,
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
  },
}));

const Home = (props) => {
  const classes = props.classes;
  const secondary = true;

  const emptyItem = <ListItem>
              <ListItemText
                primary='Loading messages'
                secondary='Please wait while we load the messages'
              />
            </ListItem>

  const allItems = props.messages.map((el, idx) => {
            return <ListItem button key={idx}>
              <ListItemText
                primary={el.subject}
                secondary={el.email.trim() !=='' ? el.email : null}
              />
            </ListItem>
          });

  const listItems = (props.messages.length === 0) ?
    emptyItem : allItems;

  return (
    <Grid container>
      <Grid item xs={12} md={6}>
        <Typography type="title" className={classes.title}>
          Messages
        </Typography>
        <div className={classes.list}>
          <List>
            {listItems}
          </List>
        </div>
      </Grid>
    </Grid>
  );
};

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(Home);