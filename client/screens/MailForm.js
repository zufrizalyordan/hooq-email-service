import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withStyles, createStyleSheet } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconBack from 'material-ui-icons/ArrowBack';
import IconSend from 'material-ui-icons/Send';
import TextField from 'material-ui/TextField';

const styleSheet = createStyleSheet('MailForm', theme => ({
  root: {
    width: '100%'
  },
  topTitle: {
    margin: '12px 0 0 10px',
    display: 'inline-block',
    fontFamily: 'Roboto',
    verticalAlign: 'top',
    textAlign: 'center',
    fontWeight: '500',
    fontSize: '1.2em',
    width: '53%'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    width: '100%'
  },
  buttonSend: {
    marginRight: 10
  }
}));

const MailForm = (props) => {
  const classes = props.classes;

  return (
    <div className={classes.root}>
      <Button
        color="primary"
        onClick={() => { props.history.goBack() }}
      >
        <IconBack />
      </Button>
      <p className={classes.topTitle}>Send New Mail</p>

      <div>
         <TextField
          id="receiver_name"
          label="Receiver Name"
          className={classes.textField}
        />

        <TextField
          id="receiver_email"
          label="Receiver Email"
          className={classes.textField}
        />

        <TextField
          id="subject"
          label="Subject"
          className={classes.textField}
        />

        <TextField
          id="multiline-static"
          label="Message"
          multiline
          rows="5"
          className={classes.textField}
          margin="normal"
        />

        <Button
          color="primary"
          onClick={() => { console.log('send request') }}
        >
        <IconSend className={classes.buttonSend}/> Send Message
      </Button>
      </div>
    </div>
  )
}

MailForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(MailForm);