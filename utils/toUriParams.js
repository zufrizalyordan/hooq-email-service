function toUriParams (objs) {
  return Object.keys(objs).map(function(key){
    return encodeURIComponent(key) + '=' + encodeURIComponent(objs[key]);
  }).join('&');
}

module.exports = toUriParams;