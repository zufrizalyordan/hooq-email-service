function isEmptyObject(obj, field) {
  const hasProp = obj.hasOwnProperty(field);
  return !(hasProp && (obj[field] !== 'undefined') && (obj[field].trim() !== ""));
}

module.exports = isEmptyObject;