##### Task story - API - Send
POST request to route api/mail, necessary params must be passed
- params are: subject, receiver info, message, sendTime
- if current provider fails to send, switch to another provider
- misc note: if all email sending fails, add email task to database for sending. best if we can hook to the mail queue service again.


##### Task story - Front end - Home
User will see a form that contains:
- Provider Selection
- subject
- receiver
- message
- time of send (optional)


### Notes
- Provider = email service provider
- Split frontend(React) and backend(using API)
- The parameters for input and output are JSON
- Frontend SPA (index.html with linked js/css) using ReactJS and other supporting packages/modules.
- Host the done App somewhere (e.g. on Amazon EC2, Heroku, Google App Engine, ZEIT Now etc.). Alternatively you may provide Docker containers with instructions on how to run them.
- Value quality over feature-completeness. It is fine to leave things aside provided you call them out in your project's README. The goal of this code sample is to help us identify what you consider production-ready code. You should consider this code ready for final review with your colleague, i.e. this would be the last step before deploying to production.

#### Assesment aspect of the code:
- Architecture: how clean is the separation between the front-end and the back-end?
- Clarity: does the README clearly and concisely explains the problem and solution? Are technical tradeoffs explained?
- Correctness: does the application do what was asked? If there is anything missing, does the README explain why it is missing?
- Code quality: is the code simple, easy to understand, and maintainable? Are there any code smells or other red flags? Does object-oriented code follows principles such as the single responsibility principle? Is the coding style consistent with the language's guidelines? Is it consistent throughout the codebase?
- Security: are there any obvious vulnerability?
- Testing: how thorough are the automated tests? Will they be difficult to change if the requirements of the application were to change? Are there some unit and some integration tests?
- We're not looking for full coverage (given time constraint) but just trying to get a feel for your testing skills.
- UX: is the web interface understandable and pleasing to use? Is the API intuitive?
- Technical choices: do choices of libraries, databases, architecture etc. seem appropriate for the chosen application?

##### Bonus points (optional items):
- Scalability: will technical choices scale well? If not, is there a discussion of those choices
in the README?
- Production-readiness: does the code include monitoring? logging? proper error handling?