// Author: zufrizalyordan@gmail.com
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let UserSchema = new Schema(
  {
    email: { type: String, required: true },
    name: { type: String, required: true },
    password: { type: String, required: true },
    status: { type: String },
    admin: { type: Boolean },
    createdAt: { type: Date, default: Date.now },
  },
  {
    versionKey: false
  }
);

// Set createdAt to current time
UserSchema.pre('save', next => {
  now = new Date();
  if(!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = mongoose.model('user', UserSchema);