// Author: zufrizalyordan@gmail.com
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let MessageSchema = new Schema(
  {
    receiver_email: { type: String, required: true },
    receiver_name: { type: String, required: true },
    subject: { type: String, required: true },
    body: { type: String, required: true },
    sendTime: { type: Date },
    status: { type: String },
    createdAt: { type: Date, default: Date.now },
  },
  {
    versionKey: false
  }
);
// since we're using the db only for saving/logging the mails, we're disabling the versionKey removes optimistic lock, the last update for the doc will always win

// Set createdAt to current time
MessageSchema.pre('save', next => {
  now = new Date();
  if(!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = mongoose.model('message', MessageSchema);