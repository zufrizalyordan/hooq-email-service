// Author: zufrizalyordan@gmail.com
'use strict';

const express    = require('express');
const app        = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const morgan = require('morgan');
const config = require('config');
const jwt = require('jsonwebtoken');
const port = process.env.PORT || 8900;

const mailRoute = require('./routes/mail');
const userRoute = require('./routes/user');
const middleware = require('./routes/middleware');

app.set('tokenSecret', config.JWT_SECRET);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.setHeader('X-Powered-By', 'PHP LoL');
  next();
})

// DB connection
const DBOptions = {
    useMongoClient: true,
};
mongoose.connect(config.DBHost, DBOptions);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// HTTP Error logging using morgan, don't show on test
if(config.util.getEnv('NODE_ENV') !== 'test') {
  let targetFile = __dirname + '/error.log';
  // Append log to file with stream
  let errorLogStream = fs.createWriteStream(targetFile,
                              { flags: 'a' }
                          );
  app.use(morgan('combined',
    { skip: function(req, res) {
        return res.statusCode < 400 // only logs or error
      },
      stream: errorLogStream
    })
  );
}

// Web App Route
app.use(express.static('dist'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/dist/index.html');
});

app.get('/setup', userRoute.setup);

// Routes for the API
let router = express.Router();

router.route('/authenticate')
  .post(userRoute.authenticate)

router.use(middleware.tokenCheck);

router.get('/', function(req, res) {
    res.json({ message: 'Welcome to Hooq Email Service' });
});

// http://localhost:8080/api/mail
router.route('/mail')
  // Send a mail with POST
  .post(mailRoute.postMessage)
  // Get all messages through GET
  .get(mailRoute.getMessages);

// API Routes will be prefixed by /api
app.use('/api', router);

// Start the server
let server = app.listen(port);
console.log('Listening on port ' + port);

module.exports = server