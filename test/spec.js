// Author: zufrizalyordan@gmail.com
// test/spec.js
process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('Web Page', () => {
  /*
  * Test the /GET route
  */
  describe('/GET index', () => {
      it('it should show index page', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
              done();
            });
      });
  });

});

describe('API', () => {
  /*
  * Test the /api/ route
  */
  const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwic2VsZWN0ZWQiOnt9LCJnZXR0ZXJzIjp7fSwiX2lkIjoiNTk3MmEwZGEwNDNlMmUwYjljMTk4N2IxIiwid2FzUG9wdWxhdGVkIjpmYWxzZSwiYWN0aXZlUGF0aHMiOnsicGF0aHMiOnsicGFzc3dvcmQiOiJpbml0IiwibmFtZSI6ImluaXQiLCJlbWFpbCI6ImluaXQiLCJjcmVhdGVkQXQiOiJpbml0IiwiYWRtaW4iOiJpbml0Iiwic3RhdHVzIjoiaW5pdCIsIl9pZCI6ImluaXQifSwic3RhdGVzIjp7Imlnbm9yZSI6e30sImRlZmF1bHQiOnt9LCJpbml0Ijp7ImNyZWF0ZWRBdCI6dHJ1ZSwiYWRtaW4iOnRydWUsInN0YXR1cyI6dHJ1ZSwicGFzc3dvcmQiOnRydWUsIm5hbWUiOnRydWUsImVtYWlsIjp0cnVlLCJfaWQiOnRydWV9LCJtb2RpZnkiOnt9LCJyZXF1aXJlIjp7fX0sInN0YXRlTmFtZXMiOlsicmVxdWlyZSIsIm1vZGlmeSIsImluaXQiLCJkZWZhdWx0IiwiaWdub3JlIl19LCJwYXRoc1RvU2NvcGVzIjp7fSwiZW1pdHRlciI6eyJkb21haW4iOm51bGwsIl9ldmVudHMiOnt9LCJfZXZlbnRzQ291bnQiOjAsIl9tYXhMaXN0ZW5lcnMiOjB9fSwiaXNOZXciOmZhbHNlLCJfZG9jIjp7ImNyZWF0ZWRBdCI6IjIwMTctMDctMjJUMDA6NDg6MjYuMzkzWiIsImFkbWluIjp0cnVlLCJzdGF0dXMiOiJhY3RpdmUiLCJwYXNzd29yZCI6IiQyYSQxMCRkbzA0WEh1V0d5YnJXeDNTdllGM20uT0hPdkFZMTNCamhFQXAuY3lqZEpTUmlHMGJoemlhcSIsIm5hbWUiOiJtaW1pbiIsImVtYWlsIjoienVmcml6YWx5b3JkYW5AZ21haWwuY29tIiwiX2lkIjoiNTk3MmEwZGEwNDNlMmUwYjljMTk4N2IxIn0sIiRpbml0Ijp0cnVlLCJpYXQiOjE1MDA2ODY4NjQsImV4cCI6MTUwMDc3MzI2NH0.H_maeVXne7r46fW5M2k6xHUzSkGxKaHAxkn34f5zpoU";

  // POST new mail
  describe('/POST mail', () => {
      it('it should show mail sent response', (done) => {
        let message = {
            receiver_email: "hooq@protonmail.com",
            receiver_name: "Test",
            subject: "test subject",
            body: "test body.",
            token: token
        }

        chai.request(server)
            .post('/api/mail')
            .send(message)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                // res.body.should.have.property('errors').eql(0);
              done();
            });
      });

      it('it should not send mail without receiver_email param', (done) => {
        let message = {
            receiver_name: "Test",
            subject: "test subject",
            body: "test body.",
            token: token
        }
        chai.request(server)
            .post('/api/mail')
            .send(message)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('receiver_email').eql('required');
              done();
            });
      });
  });

  describe('/POST authenticate', () => {
    it('it should not authenticate user with wrong email', (done) => {
        let params = {
            email: "test@mac.com",
            password: "mello"
        }
        chai.request(server)
            .post('/api/authenticate')
            .send(params)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('user').eql('not_found');
              done();
            });
      });

    it('it should not authenticate user with wrong password', (done) => {
        let params = {
            email: "zufrizalyordan@gmail.com",
            password: "mello"
        }
        chai.request(server)
            .post('/api/authenticate')
            .send(params)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('password').eql('wrong');
              done();
            });
      });

    it('it should authenticate user', (done) => {
        let params = {
            email: "zufrizalyordan@gmail.com",
            password: "tralala"
        }
        chai.request(server)
            .post('/api/authenticate')
            .send(params)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('token');
              done();
            });
      });
  });
});