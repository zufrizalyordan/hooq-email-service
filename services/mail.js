const sendMailgun = require('./sendMailgun');
const sendElasticEmail = require('./sendElasticEmail');

const isEmptyObject = require('../utils/isEmptyObject');

function validateParams(params) {
  let errors = {};

  if (isEmptyObject(params, 'receiver_name')) {
    errors['receiver_name'] = 'required';
  }

  if (isEmptyObject(params, 'receiver_email')) {
    errors['receiver_email'] = 'required';
  }
  if (isEmptyObject(params, 'subject')) {
    errors['subject'] = 'required';
  }
  if (isEmptyObject(params, 'body')) {
    errors['body'] = 'required';
  }

  return errors;
}

function send(params, res) {
  return new global.Promise(function(resolve, reject) {

    const errors = validateParams(params);

    if (Object.keys(errors).length > 0) {
      reject(errors);
    }

    const tmp = sendMailgun(params)
      .then(function (data) {
        return { message: data.message };
      })
      .catch(function(err) {
        // Failover handler #1
        return sendElasticEmail(params);
      })
      .then(function(resp) {
        let mssg = resp;
        if(resp.hasOwnProperty('message')) {
          mssg = resp.message;
        }
        return { message: mssg };
      })
      .catch(function(err) {
        return { message: err };
        // Failover handler #2 can be put here
        // build it like sendElasticEmail
        // remove return above and put in handler #4 catch
      });

      resolve(tmp);
  });
}

module.exports = { send }