const config = require('config');
const toUriParams = require('../utils/toUriParams');
const http = require('http');

function sendElasticEmail(params) {
  return new global.Promise(function(resolve, reject) {
    const mssgParams = {
      apiKey: config.ELASTICEMAIL_API_KEY,
      subject: params.subject,
      from: config.SENDER_EMAIL,
      fromName: config.SENDER_NAME,
      sender: config.SENDER_EMAIL,
      senderName: config.SENDER_NAME,
      msgTo: params.receiver_email,
      bodyHtml: params.body,
      isTransactional: true
    }

    const msgParamsString = toUriParams(mssgParams);

    const options = {
      host: 'api.elasticemail.com',
      path: `/v2/email/send?${msgParamsString}`
    };

    let str = '';
    const cb = function (res) {
      res.on('data', function(chunk) {
        str += chunk;
      });

      res.on('end', function () {
        str = JSON.parse(str);
        if (str.success === false) {
          reject(str.error);
        } else {
          resolve(str);
        }
      });

      res.on('error', function(e) {
        reject("Got error: " + e.message);
      });
    };

    http.get(options, cb).end();
  });
}

module.exports = sendElasticEmail;