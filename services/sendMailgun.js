const Mailgun = require('mailgun-js');
const config = require('config');

function sendMailgun(params) {
  const mailgunParams = {
    from: config.SENDER,
    to: params.receiver_email,
    subject: params.subject,
    html: params.body
  }

  // To simulate mailgun error/not sending,
  // you can uncomment the commented domain below,
  // and comment out the domain: config.MAILGUN_DOMAIN line
  const mailgun = new Mailgun({
    apiKey: config.MAILGUN_API_KEY,
    domain: config.MAILGUN_DOMAIN
    // domain: 'tasdg'
  });

  return mailgun.messages()
    .send(mailgunParams)
    .then(function(body) {
      if (body.statusCode > 200) {
        next(new Error('Failed to send with Mailgun'));
      }
      return body;
    })
    .catch(function(err) {
      next(new Error(err));
    });
}

module.exports = sendMailgun;